#include <Arduino.h>

#define LED 13

SIGNAL(TIMER1_COMPA_vect) {
    digitalWrite(LED, HIGH);
    delayMicroseconds(200);
    digitalWrite(LED, LOW);
}

void setup() {
    pinMode(LED, OUTPUT);

    //Timer1 nastaveni
    //Timer1 je v modu 4, preddelicka je nastavena na 8. (Citac jede od 0 do OCR1A a pak se resetuje);
    //Frekvence preruseni je (F_CPU / 8) / OCR1A
    //kde F_CPU je frekvence hodin, na Aurduino obvykle 16MHz.
    //    8 je delici pomer preddelicky, lze upravit nastavenim bitu CSxx v registru TCCR1B.
    //OCR1A maximilni hodnota citace, pri dosazeni teto hodnoty nastane preruseni a citac se automaticky resetuje a pokracuje dale od 0. (Plati pouze pro mod 4)
    TCCR1A = 0x00;
    TCCR1B = (1 << WGM12) | (1 << CS11);
    OCR1A = (F_CPU / 8) / 1000; //Pozor OCR1A je pouze 16 bit. Nutno prekontrolovat, ze se tam vysledek vejde (max 65535). 
    TIMSK1 |= (1 << OCIE1A); //Povoleni preruseni TIMER1_COMPA_vect
}

void loop() {

}
